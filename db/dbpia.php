<?php
require 'connect1.inc.php';	
session_start();
if($_SESSION["loggedIn"] != true) {
     exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="css/dbpia.css" >
	<title>Data Input</title>

</head>
<body style="overflow-x: hidden;" >
<br><br><br>


<div class="row">
	<div class="col-xs-4">
		<img src="images/student.png" class="img-responsive center-block">	
		<p class="lead text-center"> Student </p>
	</div>

	<div class="col-xs-8">
		<form class="form-horizontal" action="dbpia.php" method="post">
			<div class="form-group">
				<label for="sid" class="col-xs-2 control-label"> Sid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control"  name="sid" id="sid" placeholder="sid"></input>
				</div>
				</label>		
			</div>
			
			<div class="form-group">
				<label for="sname" class="col-xs-2 control-label"> Sname </label>
				<div class="col-xs-6">
					<input type="text" class="form-control" name="sname" id="sname" placeholder="Sname"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbatch" class="col-xs-2 control-label"> Batch </label>
				<div class="col-xs-6">
					<input type="text" class="form-control"  name="sbatch" id="sbatch" placeholder="Batch"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sgender" class="col-xs-2 control-label"> Gender </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="sgender" id="sgender" placeholder="Gender"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sprog" class="col-xs-2 control-label"> Program </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="sprog" id="sprog" placeholder="Program"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sdept" class="col-xs-2 control-label"> Dept </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="sdept" id="sdept" placeholder="Department"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sadd" class="col-xs-2 control-label"> Address </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="sadd" id="sadd" placeholder="Address"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbg" class="col-xs-2 control-label"> Blood Group</label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="sbg" id="sbg" placeholder="Blood Group"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbg" class="col-xs-2 control-label"></label>
				<div class="col-xs-6">
					<button type="submit" id="submit1"class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>		
	</div>	
</div>


<hr>


<div class="row">
	<div class="col-xs-4">
		<img src="images/faculty.png" class="img-responsive center-block">	
		<p class="lead text-center"> Faculty </p>
	</div>

	<div class="col-xs-8">
		<form class="form-horizontal" action="dbpia.php" method="post">
			<br><br><br><br><br><br><br><br><br><br>
			<div class="form-group">
				<label for="fid" class="col-xs-2 control-label"> Fid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="fid" id="fid" placeholder="Fid"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="fname" class="col-xs-2 control-label"> Fname </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="fname" id="fname" placeholder="Fname"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbg" class="col-xs-2 control-label"></label>
				<div class="col-xs-6">
					<button type="submit" id="submit2" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>		
	</div>	
</div>

<hr>


<div class="row">
	<div class="col-xs-4">
		<img src="images/course.png" class="img-responsive center-block">	
		<p class="lead text-center"> Course </p>
	</div>
	<br><br><br><br><br><br><br><br>	
	<div class="col-xs-8">
		<form class="form-horizontal" action="dbpia.php" method="post">
			<div class="form-group">
				<label for="cid" class="col-xs-2 control-label"> Cid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="cid" id="cid" placeholder="cid"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="cname" class="col-xs-2 control-label"> Cname </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="cname" id="cname" placeholder="Cname"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="cdept" class="col-xs-2 control-label"> Cdept </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="cdept" id="cdept" placeholder="Cdept"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbg" class="col-xs-2 control-label"></label>
				<div class="col-xs-6">
					<button type="submit" id="submit3" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>		
	</div>	
</div>

<hr>


<div class="row">
	<div class="col-xs-4">
		<img src="images/enroll.png" class="img-responsive center-block">	
		<p class="lead text-center"> Enrollment </p>
	</div>
	<br><br>	
	<div class="col-xs-8">
		<form class="form-horizontal" action="dbpia.php" method="post">
			<div class="form-group">
				<label for="esid" class="col-xs-2 control-label"> Sid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="esid" id="esid" placeholder="sid"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="ecid" class="col-xs-2 control-label"> Cid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="ecid" id="ecid" placeholder="cid"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="eyear" class="col-xs-2 control-label"> Year</label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="eyear" id="eyear" placeholder="Year"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="esem" class="col-xs-2 control-label"> Sem </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="esem" id="esem" placeholder="Sem"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbg" class="col-xs-2 control-label"></label>
				<div class="col-xs-6">
					<button type="submit" id="submit4" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>		
	</div>	
</div>


<hr>

<div class="row">
	<div class="col-xs-4">
		<img src="images/teaches.png" class="img-responsive center-block">	
		<p class="lead text-center"> Teaches</p>
	</div>
	<br><br><br><br><br><br><br><br><br>
	<div class="col-xs-8">
		<form class="form-horizontal" action="dbpia.php" method="post">
			<div class="form-group">
				<label for="tfid" class="col-xs-2 control-label"> Fid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="tfid" id="tfid" placeholder="Fid"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="tcid" class="col-xs-2 control-label"> Cid </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="tcid" id="tcid" placeholder="cid"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="tyear" class="col-xs-2 control-label"> Year</label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="tyear" id="tyear" placeholder="Year"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="tsem" class="col-xs-2 control-label"> Sem </label>
				<div class="col-xs-6">
					<input type="text"class="form-control" name="tsem" id="tsem" placeholder="Sem"></input>
				</div>
				</label>		
			</div>
			<div class="form-group">
				<label for="sbg" class="col-xs-2 control-label"></label>
				<div class="col-xs-6">
					<button type="submit" id="submit5" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>		
	</div>	
</div>

<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js" ></script>

</body>
</html>


<?php

	if( isset($_POST['sid']) && !empty($_POST['sid']) && isset($_POST['sname']) && !empty($_POST['sname']) && isset($_POST['sbatch']) && !empty($_POST['sbatch']) && isset($_POST['sgender']) && !empty($_POST['sgender'])  && isset($_POST['sprog']) && !empty($_POST['sprog']) && isset($_POST['sdept']) && !empty($_POST['sdept']) && isset($_POST['sadd']) && !empty($_POST['sadd']) && isset($_POST['sbg']) && !empty($_POST['sbg'])  ){

		$sid = strtolower($_POST['sid']);
		$sname = strtolower($_POST['sname']);
		$sbatch = strtolower($_POST['sbatch']);
		$sgender = strtolower($_POST['sgender']);
		$sprog = strtolower($_POST['sprog']);
		$sdept = strtolower($_POST['sdept']);
		$sadd = strtolower($_POST['sadd']);
		$sbg = strtolower($_POST['sbg']);

		$query1 = "select * from student where sid = '$sid';";
		$query_run1 = mysql_query($query1);
		
		if(mysql_num_rows($query_run1)){
			$query = "update student set sname ='$sname', sbatch= '$sbatch', sgender='$sgender', sprog='$sprog', sdept='$sdept', sadd ='$sadd', sbg = '$sbg'
			where sid = '$sid';";

			if($query_run = mysql_query($query)){
				echo '<script language="javascript">';
				echo 'alert("Successfully updated the  Student database")';
				echo '</script>';
			}
		}

		else{
			$query = "insert into student values('$sid','$sname','$sbatch','$sgender','$sprog','$sdept','$sadd','$sbg');";
			if($query_run = mysql_query($query)){
				echo '<script language="javascript">';
				echo 'alert("Successfull Entry in the Student database")';
				echo '</script>';
			}
		}

	}

	if( ( isset($_POST['fid']) && empty($_POST['fid'])) || ( isset($_POST['fname']) && empty($_POST['fname'])) || ( isset($_POST['sbatch']) && empty($_POST['sbatch'])) || ( isset($_POST['sgender']) && empty($_POST['sgender'])) || ( isset($_POST['sprog']) && empty($_POST['sprog'])) || ( isset($_POST['sdept']) && empty($_POST['sdept'])) || ( isset($_POST['sadd']) && empty($_POST['sadd'])) || ( isset($_POST['sbg']) && empty($_POST['sbg']))){
		echo '<script language="javascript">';
				echo 'alert("Please fill in all the details")';
				echo '</script>';
	}




/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */





	if( isset($_POST['fid']) && !empty($_POST['fid']) && isset($_POST['fname']) && !empty($_POST['fname']) ){

		$fid = strtolower($_POST['fid']);
		$fname = strtolower($_POST['fname']);
	

		$query1 = "select * from faculty where fid = '$fid';";
		$query_run1 = mysql_query($query1);
		
		if(mysql_num_rows($query_run1)){
			$query = "update faculty set fname ='$fname' where fid = '$fid';";

			if($query_run = mysql_query($query)){
				echo '<script language="javascript">';
				echo 'alert("Successfully updated the  Faculty database")';
				echo '</script>';
			}
		}

		else{
			$query = "insert into faculty values('$fid','$fname');";
			if($query_run = mysql_query($query)){
				echo '<script language="javascript">';
				echo 'alert("Successfull Entry in the Faculty database")';
				echo '</script>';
			}
		}

	}

	if( ( isset($_POST['fid']) && empty($_POST['fid'])) || ( isset($_POST['fname']) && empty($_POST['fname'])) ){
		echo '<script language="javascript">';
				echo 'alert("Please fill in all the details")';
				echo '</script>';
	}



/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */




	if( isset($_POST['cid']) && !empty($_POST['cid']) && isset($_POST['cname']) && !empty($_POST['cname']) && isset($_POST['cdept']) && !empty($_POST['cdept']) ){

		$cid = strtolower($_POST['cid']);
		$cname = strtolower($_POST['cname']);
		$cdept = strtolower($_POST['cdept']);
		

		$query1 = "select * from course where cid = '$cid';";
		$query_run1 = mysql_query($query1);
		
		if(mysql_num_rows($query_run1)){
			$query = "update course set cname ='$cname', cdept= '$cdept' where cid = '$cid';";

			if($query_run = mysql_query($query)){
				echo '<script language="javascript">';
				echo 'alert("Successfully updated the Course database")';
				echo '</script>';
			}
		}

		else{
			$query = "insert into course values('$cid','$cname','$cdept');";
			if($query_run = mysql_query($query)){
				echo '<script language="javascript">';
				echo 'alert("Successfull Entry in the Course database")';
				echo '</script>';
			}
		}

	}


	if( ( isset($_POST['cid']) && empty($_POST['cid'])) || ( isset($_POST['cname']) && empty($_POST['cname'])) || ( isset($_POST['cdept']) && empty($_POST['cdept']))){
		echo '<script language="javascript">';
				echo 'alert("Please fill in all the details")';
				echo '</script>';
	}


	/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */


	if( isset($_POST['esid']) && !empty($_POST['esid']) && isset($_POST['ecid']) && !empty($_POST['ecid']) && isset($_POST['eyear']) && !empty($_POST['eyear']) && isset($_POST['esem']) && !empty($_POST['esem']) ){

		$esid = strtolower($_POST['esid']);
		$ecid = strtolower($_POST['ecid']);
		$eyear = strtolower($_POST['eyear']);
		$esem = strtolower($_POST['esem']);
		
		$query1 = "select * from student where sid = '$esid';";
		$query2 = "select * from course where cid = '$ecid';";
		$query_run1 = mysql_query($query1);
		$query_run2 = mysql_query($query2);
		
		if(mysql_num_rows($query_run1) &&  mysql_num_rows($query_run2)){

			$query0 = "select * from enrollment where sid ='$esid' and cid = '$ecid';";
			$query_run0 = mysql_query($query0);

			if(mysql_num_rows($query_run0)){
				$query = "update enrollment set year ='$eyear', sem= '$esem' where sid = '$esid' and cid = '$ecid';";
				if($query_run = mysql_query($query)){
					echo '<script language="javascript">';
					echo 'alert("Successfully updated the Enrollment database")';
					echo '</script>';
				}
			}

			else{
				$query = "insert into enrollment values('$esid','$ecid','$eyear','$esem');";
				if($query_run = mysql_query($query)){
					echo '<script language="javascript">';
					echo 'alert("Successfull entry in the Enrollment database")';
					echo '</script>';
				}
			}
		}

		elseif(mysql_num_rows($query_run1) && !mysql_num_rows($query_run2)){
			echo '<script language="javascript">';
			echo 'alert("Course Record not Found")';
			echo '</script>';
		}
		
		elseif(!mysql_num_rows($query_run1) && mysql_num_rows($query_run2)) {
			echo '<script language="javascript">';
			echo 'alert("Student Record not Found")';
			echo '</script>';
		}

		else{
			echo '<script language="javascript">';
			echo 'alert("Both Student and Course Record not Found")';
			echo '</script>';
		}
	}

	if( ( isset($_POST['esid']) && empty($_POST['esid'])) || ( isset($_POST['ecid']) && empty($_POST['ecid'])) || ( isset($_POST['eyear']) && empty($_POST['eyear'])) || ( isset($_POST['esem']) && empty($_POST['esem']))){
		echo '<script language="javascript">';
				echo 'alert("Please fill in all the details")';
				echo '</script>';
	}

	/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */


	if( isset($_POST['tfid']) && !empty($_POST['tfid']) && isset($_POST['tcid']) && !empty($_POST['tcid']) && isset($_POST['tyear']) && !empty($_POST['tyear']) && isset($_POST['tsem']) && !empty($_POST['tsem']) ){

		$tfid = strtolower($_POST['tfid']);
		$tcid = strtolower($_POST['tcid']);
		$tyear = strtolower($_POST['tyear']);
		$tsem = strtolower($_POST['tsem']);
		
		$query1 = "select * from faculty where fid = '$tfid';";
		$query2 = "select * from course where cid = '$tcid';";
		$query_run1 = mysql_query($query1);
		$query_run2 = mysql_query($query2);
		
		if(mysql_num_rows($query_run1) &&  mysql_num_rows($query_run2)){

			$query0 = "select * from teaches where fid ='$tfid' and cid = '$tcid';";
			$query_run0 = mysql_query($query0);

			if(mysql_num_rows($query_run0)){
				$query = "update teaches set year ='$tyear', sem= '$tsem' where fid = '$tfid' and cid = '$tcid';";
				if($query_run = mysql_query($query)){
					echo '<script language="javascript">';
					echo 'alert("Successfully updated the Teaches database")';
					echo '</script>';
				}
			}

			else{
				$query = "insert into teaches values('$tfid','$tcid','$tyear','$tsem');";
				if($query_run = mysql_query($query)){
					echo '<script language="javascript">';
					echo 'alert("Successfull entry in the Teaches database")';
					echo '</script>';
				}
			}
		}

		elseif(mysql_num_rows($query_run1) && !mysql_num_rows($query_run2)){
			echo '<script language="javascript">';
			echo 'alert("Course Record not Found")';
			echo '</script>';
		}
		
		elseif(!mysql_num_rows($query_run1) && mysql_num_rows($query_run2)) {
			echo '<script language="javascript">';
			echo 'alert("Faculty Record not Found")';
			echo '</script>';
		}

		else{
			echo '<script language="javascript">';
			echo 'alert("Both Faculty and Course Record not Found")';
			echo '</script>';
		}
	}

	if( ( isset($_POST['tfid']) && empty($_POST['tfid'])) || ( isset($_POST['tcid']) && empty($_POST['tcid'])) || ( isset($_POST['tyear']) && empty($_POST['tyear'])) || ( isset($_POST['tsem']) && empty($_POST['tsem']))){
		echo '<script language="javascript">';
				echo 'alert("Please fill in all the details")';
				echo '</script>';
	}


?>

