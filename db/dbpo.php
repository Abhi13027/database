<?php
require 'connect1.inc.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="css/dbpo.css" >
	<title> Data Output</title>
</head>


<body style="overflow-x:hidden;">
	<br><br><br><br><br><br>
	<div class="row">	
			<form class="form-horizontal" action="dbpo.php" method="POST">	
				<div class="col-xs-2 col-xs-offset-1">
					<input type="text" class="form-control"  name="sname" id="sname" placeholder="Name"></input>	
				</div>

				<div class="col-xs-2">
					<input type="text" class="form-control"  name="sid" id="sid" placeholder="Roll No"></input>	
				</div>

				<div class="col-xs-2">
					<select class="form-control" name="sbatch">
						<option value>Batch</option>
						<option value="Y10">Y10</option>
						<option value="Y11">Y11</option>
						<option value="Y12">Y12</option>
						<option value="Y13">Y13</option>
						<option value="Y14">Y14</option>
						<option value="Y15">Y15</option>
					</select> 	
				</div>

				<div class="col-xs-2">
					<select class="form-control" name="sdept">
						<option value> Dept</option>
						<option value="ELECTRICAL ENGG."> EE </option>
						<option value="COMPUTER SCI. & ENGG."> CSE </option>
						<option value="CHEMICAL ENGG."> CHE </option>
						<option value="CIVIL ENGG."> CE </option>
						<option value="BIOL.SCI. AND BIO.ENGG."> BSBE </option>
						<option value="AEROSPACE ENGG."> AE </option>
						<option value="PHYSICS"> PHY </option>
						<option value="MATERIAL SCIENCE & ENGINEERING"> MSE </option>
						<option value="MECHANICAL ENGG."> ME </option>
						<option value="ECONOMICS">ECO </option>
						<option value="CHEMISTRY">CHM </option>
						<option value="MATHEMATICS">MTH </option>
						<option value="Math for Pg online">MSP </option>
						<option value="STATISTICS">STATS </option>
						<option value="ENGINEERING SCIENCE & MECHANIC">ES </option>
						<option value="HUMANITIES & SOC. SCIENCES">HSS </option>
					</select> 	
				</div>			

				<div class="col-xs-2">
					<select class="form-control" name="sgender">
						<option value> Gender </option>
						<option value="M"> Male</option>
						<option value="F"> Female</option>
					</select>
				</div>
				<button type="submit" id="submit1"class="btn btn-default">Submit</button>	
			</form>
	</div>
</body>
</html>

<?php



if( (isset($_POST['sbatch'])) && (isset($_POST['sgender'])) && (isset($_POST['sdept']))  && isset($_POST['sname'])  ){

	$sname  = strtolower($_POST['sname']);
	$sbatch = strtolower($_POST['sbatch']);
	$sgender= strtolower($_POST['sgender']);
	$sdept = strtolower($_POST['sdept']);
	$sid = strtolower($_POST['sid']);
	$flag = "";

	if($sbatch!=""){
		$sbatchq = "sbatch ='".$sbatch."'";
		$flag.="1";

	}

	else{
		$sbatchq="";
		$flag.="0";
	}

	if($sgender!=""){

		if($flag=="0"){
			$sgenderq = "sgender ='".$sgender."'";
			$flag.="1";
		}
		else{
			$sgenderq ="and sgender ='".$sgender."'";
			$flag.="1";
		}
	}

	else{
		$sgenderq ="";
		$flag.="0";
	}

	if($sdept!=""){
		if($flag=="00"){
			$sdeptq="sdept = '".$sdept."'";
			$flag.="1";
		}
		else{
			$sdeptq="and sdept ='".$sdept."'";
			$flag.="1";
		}
	}

	else{
		$sdeptq="";
		$flag.="0";
	}

	if($sname!=""){
		if($flag=="000"){
			$snameq="sname like '%".$sname."%'";
			$flag.="1";
		}
		else{
			$snameq="and sname like '%".$sname."%'";
			$flag.="1";
		}
	}
	else{
		$snameq="";
		$flag.="0";
	}

	if($sid!=""){
		if($flag=="0000"){
			$sidq="sid = '".$sid."'";
			$flag.="1";
		}
		else{
			$sidq="and sid = '".$sid."'";
			$flag.="1";
		}
	}
	else{
		$sidq="";
		$flag="0";
	}

	$query ="select * from student where ".$sbatchq."".$sgenderq."".$sdeptq."".$snameq."".$sidq;

		if($query_run=mysql_query($query)){
			if(mysql_num_rows($query_run)==0){
				echo " <br><br><br><br><br><br><br><br><br><br><h1 class=\"text-center\" style=\"color:#fff;\"> No results found </h1>";
			}

			else{
				echo "<br><br><br><br>";
				while($query_row = mysql_fetch_assoc($query_run)){
					$a1= ($query_row['sid']);
					$a2= ($query_row['sname']);
					$a3= ($query_row['sbatch']);
					$a4= ($query_row['sgender']);
					$a5= ($query_row['sprog']);
					$a6= ($query_row['sdept']);
					$a7= ($query_row['sadd']);
					$a8= ($query_row['sbg']);

					$a9= $a1."_0.jpg";
					$a10=strtolower($a3);

					echo "<div class=\"row\">
							<div class=\"col-xs-4 col-xs-offset-2\">
								<img src = \"student_pic/$a10/$a9\" style=\"height:200px;width:160px;\" class=\"img-responsive img-rounded\"/>
							<br><br><br><br> 
							</div>
							<div class=\"col-xs-4\">
							<div class=\"table-responsive\">
								<table class=\"table table-striped\">
									<tr>
										<td>Name</td>
										<td>$a2</td>
									</tr>
									<tr>
										<td>Roll No</td>
										<td>$a1</td>
									</tr>
									<tr>
										<td>Batch</td>
										<td>$a3</td>
									</tr>
									<tr>
										<td>Program</td>
										<td>$a5</td>
									</tr>
									<tr>
										<td>Department</td>
										<td>$a6</td>
									</tr>
									<tr>
										<td>Blood Group</td>
										<td>$a8</td>
									</tr>
								</table>
							</div>
							</div>
							<div class=\"col-xs-4\">
							</div>
						</div>";
				



				}

			}
		}

	$query = "select * from cool where sid ='$sid' order by year;";

		if($query_run=mysql_query($query)){
			if(mysql_num_rows($query_run)==0){
				echo " <br><br><br><br><br><br><br><br><br><br><h1 class=\"text-center\" style=\"color:#fff;\"> No results found </h1>";
			}


			else{

				echo "<table id=\"t1\" class=\"table table-striped\">
					<col width =\"100\">
					<col width =\"100\"> 
					<col width =\"100\">
					<col width =\"100\">
					<col width =\"100\">
					<col width =\"100\">
					<col width =\"100\">
					<col width =\"100\"> ";


				while($query_row=mysql_fetch_assoc($query_run)){
					$a1=($query_row['sid']);
					$a2=($query_row['year']);
					$a3=($query_row['sem']);
					$a4=($query_row['cid']);
					$a5=($query_row['cname']);
					$a6=($query_row['fname']);
					$a7=($query_row['dept']);

					echo "<tr>
							<td>".$a2."</td>
							<td>".$a3."</td>
							<td>".$a4."</td>
							<td>".$a5."</td>
							<td>".$a6."</td>
							<td>".$a7."</td>
							</tr>";

				}
				echo "</table>";
			}
		}

}

?>