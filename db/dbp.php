<?php
require 'connect1.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="css/dbp.css" >
	

	<title> CS315: Principles of DataBase Systems</title>
</head>

<body style="position:relative;" data-spy="scroll" data-target=".navbar">	
	
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar">	</span>
						<span class="icon-bar"></span>
					</button>

					<a href="" class="navbar-brand" id="navbar-brand"> Project DBMS</a>
				</div>

				<div class="collapse navbar-collapse" id="navigation">
					<ul class="nav navbar-nav" id="ulnav">
						<li > <a href="#outline"> Outline</a></li>
						<li > <a href="#administrator ">Administrator</a></li>
						<li > <a href="#user">User</a></li>
						<li > <a href="#team ">Team</a></li>
						<li > <a href="#contact ">Contact</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<br><br><br>	
	<div class="container" id="outline">
		<h1 class="text-center">Outline</h1>
		<br>
		<div class="row" >
			<div class="col-xs-1 col-xs-offset-3" id="col1">
				<h2 class="text-center"> C </h2>
			</div>
			<div class="col-xs-1" id="col2">
				<h2 class="text-center"> O </h2>
			</div>
			<div class="col-xs-1" id="col3">
				<h2 class="text-center"> U </h2>
			</div>
			<div class="col-xs-1" id="col4">
				<h2 class="text-center"> R </h2>
			</div>	
			<div class="col-xs-1" id="col5">
				<h2 class="text-center"> S </h2>
			</div>
			<div class="col-xs-1" id="col6">
				<h2 class="text-center"> E </h2>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2 col-xs-offset-1" id="col7">
				<h2 class="text-center"> Q </h2>
			</div>
			<div class="col-xs-2" id="col8">
				<h2 class="text-center"> U </h2>
			</div>
			<div class="col-xs-2" id="col9">
				<h2 class="text-center"> E </h2>
			</div>
			<div class="col-xs-2" id="col10">
				<h2 class="text-center"> R </h2>
			</div>	
			<div class="col-xs-2" id="col11">
				<h2 class="text-center"> Y </h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 " id="col12">
				<h2 class="text-center"> A </h2>
			</div>
			<div class="col-xs-4" id="col13">
				<h2 class="text-center"> P </h2>
			</div>
			<div class="col-xs-4" id="col14">
				<h2 class="text-center"> P </h2>
			</div>
		</div>
		

			<div class="row">
				<div class="col-xs-6 col-xs-offset-3">
					<p class="lead text-center"> <br>
					The app uses HTML, CSS and Twitter Bootstrap for the Frontend part. It uses MySQL to store the database and PHP for retrieving information from the databases.
					</p>	
				</div>
			</div>
	</div>
	<hr>

	<div class="container" id="administrator">
		<h1 class="text-center">Administrator</h1>
		<br>
		<a href="http://localhost/files/db/dbpi.php"><img src="images/admin.jpg" class="img-responsive center-block"/></a><br>
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<p class="lead text-center"> 
					The adminstrator page allows you to access the databases which are used in the project. It basically allows you to input and update data for the following tables :
				</p>	
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-2 col-xs-offset-1">
				<img src="images/student.png" class="img-responsive center-block"> 
				<h4 class="text-center"> Student </h4>
			</div>		
			
			<div class="col-xs-2">
				<img src="images/faculty.png" class="img-responsive center-block"> 
				<h4 class="text-center"> Faculty </h4>
			</div>		
			
			<div class="col-xs-2">
				<img src="images/course.png" class="img-responsive center-block"> 
				<h4 class="text-center"> Course </h4>
			</div>		
			
			<div class="col-xs-2">
				<img src="images/enroll.png" class="img-responsive center-block"> 
				<h4 class="text-center"> Enrollment </h4>
			</div>		
			
			<div class="col-xs-2">
				<img src="images/teaches.png" class="img-responsive center-block"> 
				<h4 class="text-center"> Teaches </h4>
			</div>		
			
		</div>
	</div>
	<hr>
	



	<div class="container-fluid" id="user">
		<h1 class="text-center"> User </h1> <br>
		<a href="http://localhost/files/db/dbpo.php"><img src="images/user.png" class="img-responsive center-block"/></a><br>
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<p class="lead text-center"> 
					This page allows the user to input some details about the query for which he/she wants to retrieve the information.
				</p>	
			</div>
		</div>
		<br>

	</div>

	<hr>

	<div class="container-fluid" id="team">
		<h1 class="text-center"> Team </h1> <br>
		<div class="row">
			
			<div class="col-xs-6">
				<img src="images/abhishek.jpg" class="img-circle img-responsive center-block" ><br>
				<p class="lead text-center"> Abhishek Jain <br> 3rd Year Undergraduate <br> Dept. of Economcis </p>
			</div>	

			<div class="col-xs-6">
				<img src="images/tanushree.jpg" class="img-circle img-responsive center-block" ><br>
				<p class="lead text-center"> Tanushree Gupta <br> 4th Year Undergraduate <br> Dept. of Electrical Engineering </p>
			</div>	
			
		</div>
	</div>

	<br><br><br>
	<div class="container-fluid" id="contact">
		<div class="row">
			<div class="col-xs-6">
				<h3 class="text-center">Abhishek Jain</h3>
				<p class="text-center"> Room No. 107, Hall-3 </p>
				<p class="text-center"> abhijain@iitk.ac.in</p>
				<p class="text-center"> 7754916025 </p>
			</div>

			<div class="col-xs-6">
				<h3 class="text-center">Tanushree Gupta	</h3>
				<p class="text-center">  GHT </p>
				<p class="text-center"> tshree@iitk.ac.in</p>
				<p class="text-center"> 9454803623 </p>
			</div>
		</div>	
	</div>



<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js" ></script>
</body>
</html>