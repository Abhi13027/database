<?php
require 'connect1.inc.php';
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="css/dbpi.css" >
	<title>Log In </title>
</head>

<body style="position:relative;margin: 0;padding: 0;overflow: hidden;">

<br><br><br><br>
<h1 class="text-center">Administrator <br>Log In </h1>

<br><br><br><br><br>
<div class="container">
	<form class="form-horizontal" action="dbpi.php" method="post">
  		<div class="form-group">
	    	<label for="username" class="col-xs-4 control-label">Username</label>
	    	<div class="col-xs-4">
	      		<input type="username" class="form-control" id="username" name="username" placeholder="Username">
	   		 </div>
 	 	</div>
  		

  		<div class="form-group">
    		<label for="password" class="col-xs-4 control-label">Password</label>
    			<div class="col-xs-4">
      				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
    			</div>
  		</div>
	
		<div class="form-group">
    		<div class="col-xs-offset-4 col-xs-8">
      			<div class="checkbox">
        			<label>
          				<input type="checkbox"> Remember me
        			</label>
      			</div>
    		</div>
  		</div>

  		<div class="form-group">
  			<div class="col-xs-offset-4 col-xs-8">
      			<button type="submit" class="btn btn-default">Sign in</button>
    		</div>
  		</div>
	</form>
</div>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js" ></script>
</body>
</html>


<?php

if(isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password'])){
	$username = strtolower($_POST['username']);
	$password = strtolower($_POST['password']);

	$query = "select * from admin where username = '$username' and password = '$password';";
	$query_run = mysql_query($query);

	if(mysql_num_rows($query_run)){
    		$_SESSION["loggedIn"] = true;
    		$_SESSION["username"] = $username;
    		header('Location: http://localhost/files/db/dbpia.php'); 
	}

	else{
		echo '<script language="javascript">';
		echo 'alert("Invalid Username or Password")';
		echo '</script>';
	}
}




?>